#
# Cookbook:: custom_apache
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

secret = Chef::EncryptedDataBagItem.load_secret("/etc/chef/encrypted_data_bag_secret")
all_var_values = Chef::EncryptedDataBagItem.load("mysecrets", "marioworld", secret)

#force_default["myvar"]="abc"
#force_default["dbpassword"]="sss123hhh"

# step 1 - installing apache webserver
package 'httpd' do
  action :install
end

# Step 2 - we copy index.html file default docroot apache (/var/www/html)
template "/var/www/html/index.html" do
  variables(:dbserver => all_var_values['db'],
            :databasename => all_var_values['dbname'],
            :username => all_var_values['dbusername'],
            :password => all_var_values['dbpassword']
            )
  source 'index.html.erb'
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[httpd]', :delayed
end

# Step 3 - starting and enabling service 
service 'httpd' do
  supports restart: true
  action :enable
end
